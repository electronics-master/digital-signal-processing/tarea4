time = 10; # In s
Fs = 8000; # In Hz

k_1 = Fs * 0.050;
a_1 = 0.6;
k_2 = Fs * 0.250;
a_2 = 0.4;
k_3 = Fs * 0.500;
a_3 = 0.2;

x1_coeff = [1-a_1];
x2_coeff = [1-a_2];
x3_coeff = [1-a_3];
  
y1_coeff = zeros(1, k_1);
y1_coeff(1) = 1;
y1_coeff(k_1) = -a_1;

y2_coeff = zeros(1, k_2);
y2_coeff(1) = 1;
y2_coeff(k_2) = -a_2;

y3_coeff = zeros(1, k_3);
y3_coeff(1) = 1;
y3_coeff(k_3) = -a_3;

# Impulse
impulse = zeros(1, 10*Fs);
impulse(1) = 1;

filtered_1 = filter(x1_coeff, y1_coeff, impulse);
filtered_2 = filter(x2_coeff, y2_coeff, impulse);
filtered_3 = filter(x3_coeff, y3_coeff, impulse);

h = plot(filtered_1);
hold on;
j = plot(filtered_2);
hold on;
k = plot(filtered_3);
hold on;
waitfor(h);
waitfor(j);
waitfor(k);
