# Tarea 4 - Audio en C

## Procesamiento desde micrófono

```bash
./jack
```

## Procesamiento desde archivo

```bash
./jack -c < test.wav
```
